import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { MotivatorComponent } from './motivator/motivator';

@NgModule({
	declarations: [MotivatorComponent],
	imports: [IonicModule],
	exports: [MotivatorComponent]
})
export class ComponentsModule {}
