import { Component } from '@angular/core';

@Component({
  selector: 'motivator',
  templateUrl: 'motivator.html'
})
export class MotivatorComponent {

  quotes: string[] = [];
  currentQuote: string;

  constructor() {
    
      this.quotes = [
        "The worst thing I can be is the same as everybody else. - Arnold Schwarzenegger",
        "Strength does not come from winning. Your struggles develop your strengths. - Arnold Schwarzenegger",
        "The pessimist sees difficulty in every opportunity. The optimist sees the opportunity in every difficulty. - Winston Churchill",
        "We choose to go to the moon in this decade and do the other things, not because they are easy, but because they are hard. - John F Kennedy",
        "Don't let yesterday take up too much of today. - Will Rodgers",
        "Creativity is intelligence having fun. - Albert Einstein",
        "Do what you can, with all you have, wherever you are. - Theodore Roosevelt"
      ];

  }

  ngOnInit(){
    this.setRandomQuote();
  }


  setRandomQuote() {

    let randomIndex = Math.floor(Math.random() * this.quotes.length);

    this.currentQuote = this.quotes[randomIndex];

  }

}
